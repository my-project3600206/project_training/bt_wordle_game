﻿
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using WordleGame;

namespace Wordle
{
    class Program
    {
        public static void Main(string[] args)
        {
            // check game
            string checknGame;
            Console.WriteLine("-----------------------------------------------------------");
            Console.WriteLine("---------My Wordle!----------");
            Console.WriteLine("---Guess the Wordle in 6 tries---");
            do
            {
                Console.WriteLine("Would you like to play My Wordle [y|n]?");
                checknGame = Console.ReadLine();
                if (checknGame == "n")
                {
                    Console.WriteLine("No worries... another time perhaps... :)");
                }
            }
            while(checknGame != "y");
            WordlePlayGame wordlePlayGame = new WordlePlayGame();
             wordlePlayGame.playGame();
        }
        
    }
    
}