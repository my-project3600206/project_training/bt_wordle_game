﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WordleGame
{
    public class PlayGameResult
    {
        public ArrayList correctLetterList;
        public ArrayList usedLetterList;
        public bool isResult;
        public PlayGameResult()
        {
            this.correctLetterList = new ArrayList();
            this.usedLetterList = new ArrayList();
            this.isResult = false;
        }
    }
}
