﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WordleGame;

namespace WordleGame
{
    public class WordlePlayGame
    {
        Data data = new Data();
        string wordGess;
        int numberPlaying = 0;
        int numberGess = 0;
        string playAgain;
        int wordleCorrect = 0;
        int wordleInCorrect = 0;
        ArrayList correctLetters = new ArrayList();
        ArrayList usedLetters = new ArrayList();
        string randomResult = "";
       public WordlePlayGame()
       {
            this.randomResult = data.randomWord();
       }

        public PlayGameResult playingGame()
        {
            List<char> listResult = wordGess.ToCharArray().ToList();
            List<char> listRandomModify = randomResult.ToCharArray().ToList();
           
            bool isResult;
            int correctSpot =0;
            int wrongSpot =0;

            PlayGameResult playGameResult = new PlayGameResult();
            Console.WriteLine("word gess: " + wordGess);
            Console.WriteLine( wordGess);
            for (int i =0; i< wordGess.Length; i++)
            {
                if (wordGess[i] == randomResult[i])
                {
                    correctSpot += 1;
                    listRandomModify[i] = '^';
                    listResult[i] = '^';
                    usedLetters.Remove(wordGess[i]);

                    if (!correctLetters.Contains(wordGess[i]))
                    {
                        correctLetters.Add(wordGess[i]);
                    }
                }
                else if (!randomResult.Contains(wordGess[i]))
                {
                    listResult[i] = '-';

                    if (!usedLetters.Contains(wordGess[i]) && !correctLetters.Contains(wordGess[i]))
                    {
                        usedLetters.Add(wordGess[i]);
                    }
                }
            }
            for(int i =0; i < wordGess.Length; i++)
            {
                if (listRandomModify.Contains(listResult[i]) && listResult[i] != '^' && listResult[i] != '-' && listResult[i] != '*')
                {
                    int index = listRandomModify.IndexOf(listResult[i]);
                    listRandomModify[index] = '*';
                    listResult[i] = '*';
                    wrongSpot += 1;

                    if (!usedLetters.Contains(wordGess[i]) && !correctLetters.Contains(wordGess[i]))
                    {
                        usedLetters.Add(wordGess[i]);
                    }
                }
                else if (listResult[i] != '^' && listResult[i] != '-' && listResult[i] != '*')
                {
                    listResult[i] = '-';
                }
            }
            foreach (var item in listResult)
            {

                Console.Write(item.ToString());
            }
            Console.WriteLine("");
            Console.WriteLine("CorrectSpot (^) : " + correctSpot);
            Console.WriteLine("WrongSpot (*) : " + wrongSpot);
            Console.Write(Environment.NewLine);
            Console.Write("Correct Letters: ");

            foreach (var item in correctLetters)
            {

                Console.Write(item.ToString() + " ");
            }
            Console.Write(Environment.NewLine);
            Console.Write("Used Letters: ");

            foreach (var item in usedLetters)
            {

                Console.Write(item.ToString() + " ");
            }

            playGameResult.correctLetterList = correctLetters;
            playGameResult.usedLetterList = usedLetters;
            playGameResult.isResult = (new string(listRandomModify.ToArray()) == "^^^^^");
            Console.Write(Environment.NewLine);
            Console.WriteLine( "return:" + playGameResult.isResult);
            return playGameResult;
        }

        public bool validateWordle(string wordGess) 
        {
            if(wordGess.Length == 5 && !data.wordList.Contains(wordGess))
            {
                Console.WriteLine("Not in word list!");
                return false;
            }
            else if (wordGess.Length != 5)
            {
                Console.WriteLine("Five letter words only please.");
                return false;
            }
            else
            {
                return true;
            }
           
            
        }
        public void playGame()
        {
            PlayGameResult playGameResult = new PlayGameResult();
            while (numberGess <= 6 && playGameResult.isResult == false)
            {
                bool checkWord;
                
                do
                {
                    Console.WriteLine("------------------------------------");
                    Console.WriteLine("Word is: " + randomResult);
                    Console.Write("Please enter your guess - attempt " + (numberGess + 1) + " : ");
                    wordGess = Console.ReadLine();

                    checkWord = validateWordle(wordGess);

                } while (checkWord == false);

                numberGess += 1;

                playGameResult = playingGame();

                if(playGameResult.isResult == true) 
                {
                    numberPlaying = numberPlaying + 1;
                    Console.WriteLine("Correct letters: " + randomResult);
                    Console.WriteLine("Solved in " + numberGess + " tries! Well done!");
                    Console.WriteLine("Would you like play again [y/n]");
                    wordleCorrect += 1;
                    playAgain = Console.ReadLine();

                    if (playAgain == "y")
                    {
                        numberGess = 0;
                         correctLetters = new ArrayList();
                         usedLetters = new ArrayList();
                        playGameResult.isResult = false;
                        randomResult = data.randomWord();

                        playGame();
                    }
                    else 
                    {
                        // wordleCorrect += 1;
                        Console.WriteLine(" You played " + numberPlaying + " game: ");
                        Console.WriteLine("--> Number of wordles solved: " + wordleCorrect);
                        Console.WriteLine("--> Number of wordles unSolved: " + wordleInCorrect);
                        return;
                    }

                }
                if (numberGess == 6 && playGameResult.isResult == false)
                {
                    Console.WriteLine("Oh no! Better luck next time!");
                    Console.WriteLine("The wordle was: " + randomResult);
                    numberPlaying = numberPlaying + 1;

                    Console.WriteLine("Would you like play again [y/n]");
                    wordleInCorrect += 1;
                    playAgain = Console.ReadLine();

                    if (playAgain == "y")
                    {
                        numberGess = 0;
                        randomResult = data.randomWord();
                         correctLetters = new ArrayList();
                         usedLetters = new ArrayList();
                        playGame();
                    }
                    else
                    {
                        Console.WriteLine(" You played " + numberPlaying + " game: ");
                        Console.WriteLine("--> Number of wordles solved: " + wordleCorrect);
                        Console.WriteLine("--> Number of wordles unSolved: " + wordleInCorrect);
                        return;
                    }

                }
            }

        }
       
    }
}
